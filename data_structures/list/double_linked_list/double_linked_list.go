package double_linked_list

type (
	DoubleLinkedList struct {
		head *node
		tail *node
		size int
	}

	node struct {
		data interface{}
		prev *node
		next *node
	}
)

func (d *DoubleLinkedList) Insert(idx int, value interface{}) (error, int) {
	return nil, 0
}

func (d *DoubleLinkedList) Get(idx int) (error, interface{}) {
	return nil, nil
}

func (d *DoubleLinkedList) Set(idx int, value interface{}) error {
	return nil
}

func (d *DoubleLinkedList) Delete(idx int) error {
	return nil
}

func (d *DoubleLinkedList) Len() int {
	return 0
}
