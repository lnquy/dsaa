package list

type List interface {
	Insert(idx int, value interface{}) (error, int)
	Get(idx int) (error, interface{})
	Set(idx int, value interface{}) error
	Delete(idx int) error
	Len() int
}
