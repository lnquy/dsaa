package double_ended_queue

type (
	DoubleEndedQueue struct {
		head *node
		tail *node
		size int
	}

	node struct {
		data interface{}
		prev *node
		next *node
	}
)

func (q *DoubleEndedQueue) Enqueue(data interface{}, isHead bool) error {
	return nil
}

func (q *DoubleEndedQueue) Dequeue(isHead bool) (interface{}, error) {
	return nil, nil
}

