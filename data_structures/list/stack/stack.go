package stack

type (
	Stack struct {
		head *node
		size int
	}

	node struct {
		data interface{}
		next *node
	}
)

func (s *Stack) Push(data interface{}) error {
	return nil
}

func (s *Stack) Pop() (interface{}, error) {
	return nil, nil
}
