package queue

type (
	Queue struct {
		head *node
		tail *node
		size int
	}

	node struct {
		data interface{}
		prev *node
		next *node
	}
)

func (q *Queue) Enqueue(data interface{}) error {
	return nil
}

func (q *Queue) Dequeue() (interface{}, error) {
	return nil, nil
}
