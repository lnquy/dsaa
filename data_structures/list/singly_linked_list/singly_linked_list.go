package singly_linked_list

import (
	"fmt"
	"errors"
)

type (
	SinglyLinkedList struct {
		head *node
		size int
	}

	node struct {
		value interface{}
		next  *node
	}
)

func (s *SinglyLinkedList) Insert(idx int, value interface{}) (error, int) {
	// Insert last
	if idx < 0 || idx == s.size {
		if s.head == nil {
			s.head = &node{value: value}
			s.size = 1
			return nil, 0
		}
		n := s.head
		iidx := 0
		for n.next != nil {
			n = n.next
			iidx++
		}
		n.next = &node{value: value}
		s.size++
		return nil, iidx+1
	}

	// Insert first
	if idx == 0 {
		n := &node{
			value: value,
			next:  s.head,
		}
		s.head = n
		s.size++
		return nil, 0
	}

	// Insert middle
	if s.size < idx {
		return fmt.Errorf("index out of bound: %d/%d", idx, s.size), -1
	}
	n := s.head
	for i := 0; i < idx; i++ {
		n = n.next
	}
	newNode := &node{
		value: value,
		next: n.next,
	}
	n.next = newNode
	s.size++
	return nil, idx+1
}

func (s *SinglyLinkedList) Get(idx int) (error, interface{}) {
	if s.size == 0 {
		return errors.New("empty linked list"), nil
	}
	if idx < 0 || idx > s.size {
		return fmt.Errorf("index out of bound: %d/%d", idx, s.size), nil
	}

	n := s.head
	for i := 0; i < idx; i++ {
		n = n.next
	}
	return nil, n.value
}

func (s *SinglyLinkedList) Set(idx int, value interface{}) error {
	if s.size == 0 {
		return errors.New("empty linked list")
	}
	if idx < 0 || idx > s.size {
		return fmt.Errorf("index out of bound: %d/%d", idx, s.size)
	}

	n := s.head
	for i := 0; i < idx; i++ {
		n = n.next
	}
	n.value = value
	return nil
}

func (s *SinglyLinkedList) Delete(idx int) error {
	if s.size == 0 {
		return errors.New("empty linked list")
	}
	if idx < 0 || idx > s.size {
		return fmt.Errorf("index out of bound: %d/%d", idx, s.size)
	}

	// Delete first
	if idx == 0 {
		if s.size == 0 {
			s.head = nil
			return nil
		}

		s.head = s.head.next
		s.size--
		return nil
	}

	// Delete middle/last
	n := s.head
	for i := 0; i < idx -1; i++ {
		n = n.next
	}
	if n.next.next == nil {
		n.next = nil
		s.size--
		return nil
	}
	n.next = n.next.next
	s.size--
	return nil
}

func (s *SinglyLinkedList) Len() int {
	return s.size
}

func (s *SinglyLinkedList) toArray() []interface{} {
	ret := make([]interface{}, 0)

	n := s.head
	for n != nil {
		ret = append(ret, n.value)
		n = n.next
	}
	return ret
}
