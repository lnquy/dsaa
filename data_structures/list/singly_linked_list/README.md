# Singly Linked List
Singly Linked List is the collection of nodes in which each node stores the actual value and a pointer links to the next node.
![ssl-wiki](_misc/612px-Singly-linked-list.png)

Time complexity:

- Access: $O(n)$
- Search: $O(n)$
- Insertion: $O(1)$
- Deletion: $O(1)$
Note: The insertion and deletion assumed that we already stood on the node where operation will take action on. 
So the actual complexity of insertion and deletion executions must include the access complexity which means: $O(n)$.

Space complexity: $O(n)$.  
Remember each node is overheaded by 1 pointer (8 bytes). So the actual memory usage is: $(S + 8)*n$, with $S$ is the size of value each node stored.

##### Cons:

- Can fit on fragmented memory where there's not enough continuous memory blocks to allocate.
- Fast insertion and deletion.
- Small overhead.

##### Pros:

- Slow access => Skip list.
- May not take advantage of CPU cache.
- Overheaded (compare to array).


