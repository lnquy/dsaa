package singly_linked_list

import (
	"testing"
	"errors"
	"reflect"
)

func TestSinglyLinkedList_Insert(t *testing.T) {
	tcs := []struct {
		inIndex  int
		inValue  int
		outErr   error
		outIndex int
		outArray []interface{}
	}{
		{inIndex: -1, inValue: 0, outErr: nil, outIndex: 0, outArray: []interface{}{0}},
		{inIndex: -1, inValue: 1, outErr: nil, outIndex: 1, outArray: []interface{}{0, 1}},
		{inIndex: -1, inValue: 2, outErr: nil, outIndex: 2, outArray: []interface{}{0, 1, 2}},
		{inIndex: 0, inValue: 3, outErr: nil, outIndex: 0, outArray: []interface{}{3, 0, 1, 2}},
		{inIndex: 1, inValue: 4, outErr: nil, outIndex: 2, outArray: []interface{}{3, 0, 4, 1, 2}},
		{inIndex: 3, inValue: 5, outErr: nil, outIndex: 4, outArray: []interface{}{3, 0, 4, 1, 5, 2}},
		{inIndex: 0, inValue: 6, outErr: nil, outIndex: 0, outArray: []interface{}{6, 3, 0, 4, 1, 5, 2}},
		{inIndex: -1, inValue: 7, outErr: nil, outIndex: 7, outArray: []interface{}{6, 3, 0, 4, 1, 5, 2, 7}},
		{inIndex: 10, inValue: 8, outErr: errors.New("index out of bound: 10/8"), outIndex: -1, outArray: []interface{}{6, 3, 0, 4, 1, 5, 2, 7}},
		{inIndex: 5, inValue: 9, outErr: nil, outIndex: 6, outArray: []interface{}{6, 3, 0, 4, 1, 5, 9, 2, 7}},
		{inIndex: -1, inValue: 10, outErr: nil, outIndex: 9, outArray: []interface{}{6, 3, 0, 4, 1, 5, 9, 2, 7, 10}},
		{inIndex: 10, inValue: 11, outErr: nil, outIndex: 10, outArray: []interface{}{6, 3, 0, 4, 1, 5, 9, 2, 7, 10, 11}},
		{inIndex: 0, inValue: 12, outErr: nil, outIndex: 0, outArray: []interface{}{12, 6, 3, 0, 4, 1, 5, 9, 2, 7, 10, 11}},
		{inIndex: 20, inValue: 13, outErr: errors.New("index out of bound: 20/12"), outIndex: -1, outArray: []interface{}{12, 6, 3, 0, 4, 1, 5, 9, 2, 7, 10, 11}},
	}

	sll := SinglyLinkedList{}
	for tcIdx, tc := range tcs {
		retErr, retIdx := sll.Insert(tc.inIndex, tc.inValue)

		if (tc.outErr == nil && retErr != nil) || (tc.outErr != nil && retErr == nil) {
			t.Errorf("%d. Insert(%d, %v) = (%v, %d). Expected error: %v. Got: %v",
				tcIdx, tc.inIndex, tc.inValue, retErr, retIdx, tc.outErr, retErr)
		}
		if retErr != nil && tc.outErr != nil && retErr.Error() != tc.outErr.Error() {
			t.Errorf("%d. Insert(%d, %v) = (%v, %d). Expected error: %v. Got: %v",
				tcIdx, tc.inIndex, tc.inValue, retErr, retIdx, tc.outErr, retErr)
		}
		if retIdx != tc.outIndex {
			t.Errorf("%d. Insert(%d, %v) = (%v, %d). Expected index: %d. Got: %d",
				tcIdx, tc.inIndex, tc.inValue, retErr, retIdx, tc.outIndex, retIdx)
		}
		if !reflect.DeepEqual(tc.outArray, sll.toArray()) {
			t.Errorf("%d. Insert(%d, %v) = (%v, %d). Expected array: %v. Got: %v",
				tcIdx, tc.inIndex, tc.inValue, retErr, retIdx, tc.outArray, sll.toArray())
		}
	}
}

func TestSinglyLinkedList_Get(t *testing.T) {
	sll := SinglyLinkedList{}
	sll.Insert(-1, 0)
	sll.Insert(-1, 1)
	sll.Insert(-1, 2)
	sll.Insert(-1, 3)
	sll.Insert(-1, 4)
	sll.Insert(-1, 5)
	sll.Insert(-1, 6)

	tcs := []struct {
		inIndex  int
		outErr   error
		outValue interface{}
	}{
		{inIndex: 0, outErr: nil, outValue: 0},
		{inIndex: 1, outErr: nil, outValue: 1},
		{inIndex: 2, outErr: nil, outValue: 2},
		{inIndex: 3, outErr: nil, outValue: 3},
		{inIndex: 4, outErr: nil, outValue: 4},
		{inIndex: 5, outErr: nil, outValue: 5},
		{inIndex: 6, outErr: nil, outValue: 6},
		{inIndex: -1, outErr: errors.New("index out of bound: -1/7"), outValue: nil},
		{inIndex: -5, outErr: errors.New("index out of bound: -5/7"), outValue: nil},
		{inIndex: 10, outErr: errors.New("index out of bound: 10/7"), outValue: nil},
		{inIndex: 200, outErr: errors.New("index out of bound: 200/7"), outValue: nil},
	}

	for tcIdx, tc := range tcs {
		retErr, retValue := sll.Get(tc.inIndex)

		if (tc.outErr != nil && retErr == nil) || (tc.outErr == nil && retErr != nil) {
			t.Errorf("%d. Get(%d) = (%v, %d). Expected error: %v. Got: %v", tcIdx, tc.inIndex, retErr, retValue, tc.outErr, retErr)
		}
		if tc.outErr != nil && retErr != nil && tc.outErr.Error() != retErr.Error() {
			t.Errorf("%d. Get(%d) = (%v, %d). Expected error: %v. Got: %v", tcIdx, tc.inIndex, retErr, retValue, tc.outErr, retErr)
		}
		if !reflect.DeepEqual(tc.outValue, retValue) {
			t.Errorf("%d. Get(%d) = (%v, %d). Expected value: %v. Got: %v", tcIdx, tc.inIndex, retErr, retValue, tc.outValue, retValue)
		}
	}
}

func TestSinglyLinkedList_Set(t *testing.T) {
	sll := SinglyLinkedList{}
	sll.Insert(-1, 0)
	sll.Insert(-1, 1)
	sll.Insert(-1, 2)
	sll.Insert(-1, 3)
	sll.Insert(-1, 4)
	sll.Insert(-1, 5)
	sll.Insert(-1, 6)

	tcs := []struct {
		inIndex  int
		inValue  int
		outErr   error
		outArray []interface{}
	}{
		{inIndex: 0, inValue: 10, outErr: nil, outArray: []interface{}{10, 1, 2, 3, 4, 5, 6}},
		{inIndex: 2, inValue: 12, outErr: nil, outArray: []interface{}{10, 1, 12, 3, 4, 5, 6}},
		{inIndex: 3, inValue: 13, outErr: nil, outArray: []interface{}{10, 1, 12, 13, 4, 5, 6}},
		{inIndex: 6, inValue: 16, outErr: nil, outArray: []interface{}{10, 1, 12, 13, 4, 5, 16}},
		{inIndex: -1, inValue: 8, outErr: errors.New("index out of bound: -1/7"), outArray: []interface{}{10, 1, 12, 13, 4, 5, 16}},
		{inIndex: 10, inValue: 8, outErr: errors.New("index out of bound: 10/7"), outArray: []interface{}{10, 1, 12, 13, 4, 5, 16}},
	}

	for tcIdx, tc := range tcs {
		retErr := sll.Set(tc.inIndex, tc.inValue)

		if (tc.outErr == nil && retErr != nil) || (tc.outErr != nil && retErr == nil) {
			t.Errorf("%d. Set(%d, %v) = (%v). Expected error: %v. Got: %v",
				tcIdx, tc.inIndex, tc.inValue, retErr, tc.outErr, retErr)
		}
		if retErr != nil && tc.outErr != nil && retErr.Error() != tc.outErr.Error() {
			t.Errorf("%d. Set(%d, %v) = (%v). Expected error: %v. Got: %v",
				tcIdx, tc.inIndex, tc.inValue, retErr, tc.outErr, retErr)
		}
		if !reflect.DeepEqual(tc.outArray, sll.toArray()) {
			t.Errorf("%d. Set(%d, %v) = (%v). Expected array: %v. Got: %v",
				tcIdx, tc.inIndex, tc.inValue, retErr, tc.outArray, sll.toArray())
		}
	}
}

func TestSinglyLinkedList_Delete(t *testing.T) {
	sll := SinglyLinkedList{}
	sll.Insert(-1, 0)
	sll.Insert(-1, 1)
	sll.Insert(-1, 2)
	sll.Insert(-1, 3)
	sll.Insert(-1, 4)
	sll.Insert(-1, 5)
	sll.Insert(-1, 6)

	tcs := []struct {
		inIndex  int
		outErr   error
		outArray []interface{}
	}{
		{inIndex: -1, outErr: errors.New("index out of bound: -1/7"), outArray: []interface{}{0, 1, 2, 3, 4, 5, 6}},
		{inIndex: 0, outErr: nil, outArray: []interface{}{1, 2, 3, 4, 5, 6}},
		{inIndex: 2, outErr: nil, outArray: []interface{}{1, 2, 4, 5, 6}},
		{inIndex: 3, outErr: nil, outArray: []interface{}{1, 2, 4, 6}},
		{inIndex: 3, outErr: nil, outArray: []interface{}{1, 2, 4}},
		{inIndex: 10, outErr: errors.New("index out of bound: 10/3"), outArray: []interface{}{1, 2, 4}},
	}

	for tcIdx, tc := range tcs {
		retErr := sll.Delete(tc.inIndex)

		if (tc.outErr == nil && retErr != nil) || (tc.outErr != nil && retErr == nil) {
			t.Errorf("%d. Delete(%d) = (%v). Expected error: %v. Got: %v",
				tcIdx, tc.inIndex, retErr, tc.outErr, retErr)
		}
		if retErr != nil && tc.outErr != nil && retErr.Error() != tc.outErr.Error() {
			t.Errorf("%d. Delete(%d) = (%v). Expected error: %v. Got: %v",
				tcIdx, tc.inIndex, retErr, tc.outErr, retErr)
		}
		if !reflect.DeepEqual(tc.outArray, sll.toArray()) {
			t.Errorf("%d. Delete(%d) = (%v). Expected array: %v. Got: %v",
				tcIdx, tc.inIndex,retErr, tc.outArray, sll.toArray())
		}
	}
}
