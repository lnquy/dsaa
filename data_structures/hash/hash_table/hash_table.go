package hash_table

type (
	HashTable struct {
		buckets []bucket
		length  int
	}

	bucket [8]node

	node struct {
		key   int64
		value interface{}
	}
)

func (h *HashTable) Insert(key int, value interface{}) error {
	return nil
}

func (h *HashTable) Get(key int) (interface{}, error) {
	return nil, nil
}

func (h *HashTable) Delete(key int) error {
	return nil
}

func (h *HashTable) Len() int {
	return h.length
}
