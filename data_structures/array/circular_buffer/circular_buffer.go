package circular_buffer

type CircularBuffer struct {
	buff   []interface{}
	length int
	start  int
	end    int
}

func (c *CircularBuffer) Push(data interface{}) (int, error) {
	return 0, nil
}

func (c *CircularBuffer) Pop() (interface{}, error) {
	return nil, nil
}

func (c *CircularBuffer) Len() int {
	return c.length
}
