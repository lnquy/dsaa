# Array
Array is a continuous memory block containing fixed-size element(s).  
![array-wiki]()

Time complexity:

- Access: $O(1)$
- Search: $O(n)$
- Insertion: $O(n)$
- Deletion: $O(n)$

Space complexity: $O(n)$.  

##### Cons:

- Fastest access.
- No overhead.
- Take advantage of CPU cache.

##### Pros:

- May not fit on fragmented memory.
- Slow insertion and deletion.

##### Notes:
- Access the invalid index of array will cause program to crash in almost (if not) all programming languages.
