package bit

import "testing"

func TestIsOneBit(t *testing.T) {
	tcs := []struct {
		b        byte
		index    byte
		expected bool
	}{
		// 0: 0000 0000
		{b: 0, index: 0, expected: false},
		{b: 0, index: 1, expected: false},
		{b: 0, index: 2, expected: false},
		{b: 0, index: 3, expected: false},
		{b: 0, index: 4, expected: false},
		{b: 0, index: 5, expected: false},
		{b: 0, index: 6, expected: false},
		{b: 0, index: 7, expected: false},
		// 57: 0011 1001
		{b: 57, index: 0, expected: true},
		{b: 57, index: 1, expected: false},
		{b: 57, index: 3, expected: true},
		{b: 57, index: 2, expected: false},
		{b: 57, index: 4, expected: true},
		{b: 57, index: 5, expected: true},
		{b: 57, index: 6, expected: false},
		{b: 57, index: 7, expected: false},
		// 167: 1010 0111
		{b: 167, index: 0, expected: true},
		{b: 167, index: 1, expected: true},
		{b: 167, index: 2, expected: true},
		{b: 167, index: 3, expected: false},
		{b: 167, index: 4, expected: false},
		{b: 167, index: 5, expected: true},
		{b: 167, index: 6, expected: false},
		{b: 167, index: 7, expected: true},
		// 255: 1111 1111
		{b: 255, index: 0, expected: true},
		{b: 255, index: 1, expected: true},
		{b: 255, index: 2, expected: true},
		{b: 255, index: 3, expected: true},
		{b: 255, index: 4, expected: true},
		{b: 255, index: 5, expected: true},
		{b: 255, index: 6, expected: true},
		{b: 255, index: 7, expected: true},
	}

	for idx, tc := range tcs {
		ret := IsOneBit(tc.b, tc.index)
		if ret != tc.expected {
			t.Errorf("#%v: IsOneBit(%v, %v) = %v. Expected: %v", idx, tc.b, tc.index, ret, tc.expected)
		}
	}
}

var _dumpIsOneBit bool

func BenchmarkIsOneBit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_dumpIsOneBit = IsOneBit(0, 0)
	}
}

func TestSetOneBit(t *testing.T) {
	tcs := []struct {
		b        byte
		index    byte
		expected byte
	}{
		// 0: 0000 0000
		{b: 0, index: 0, expected: 1},
		{b: 0, index: 1, expected: 2},
		{b: 0, index: 2, expected: 4},
		{b: 0, index: 3, expected: 8},
		{b: 0, index: 4, expected: 16},
		{b: 0, index: 5, expected: 32},
		{b: 0, index: 6, expected: 64},
		{b: 0, index: 7, expected: 128},
		// 57: 0011 1001
		{b: 57, index: 0, expected: 57},
		{b: 57, index: 1, expected: 59},
		{b: 57, index: 2, expected: 61},
		{b: 57, index: 3, expected: 57},
		{b: 57, index: 4, expected: 57},
		{b: 57, index: 5, expected: 57},
		{b: 57, index: 6, expected: 121},
		{b: 57, index: 7, expected: 185},
		// 255: 1111 1111
		{b: 255, index: 0, expected: 255},
		{b: 255, index: 1, expected: 255},
		{b: 255, index: 2, expected: 255},
		{b: 255, index: 3, expected: 255},
		{b: 255, index: 4, expected: 255},
		{b: 255, index: 5, expected: 255},
		{b: 255, index: 6, expected: 255},
		{b: 255, index: 7, expected: 255},
	}

	for idx, tc := range tcs {
		ret := SetOneBit(tc.b, tc.index)
		if ret != tc.expected {
			t.Errorf("#%v: SetOneBit(%v, %v) = %v. Expected: %v", idx, tc.b, tc.index, ret, tc.expected)
		}
	}
}

var _dumpSetOneBit byte

func BenchmarkSetOneBit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_dumpSetOneBit = SetOneBit(0, 0)
	}
}

func TestSetZeroBit(t *testing.T) {
	tcs := []struct {
		b        byte
		index    byte
		expected byte
	}{
		// 0: 0000 0000
		{b: 0, index: 0, expected: 0},
		{b: 0, index: 1, expected: 0},
		{b: 0, index: 2, expected: 0},
		{b: 0, index: 3, expected: 0},
		{b: 0, index: 4, expected: 0},
		{b: 0, index: 5, expected: 0},
		{b: 0, index: 6, expected: 0},
		{b: 0, index: 7, expected: 0},
		// 57: 0011 1001
		{b: 57, index: 0, expected: 56},
		{b: 57, index: 1, expected: 57},
		{b: 57, index: 2, expected: 57},
		{b: 57, index: 3, expected: 49},
		{b: 57, index: 4, expected: 41},
		{b: 57, index: 5, expected: 25},
		{b: 57, index: 6, expected: 57},
		{b: 57, index: 7, expected: 57},
		// 255: 1111 1111
		{b: 255, index: 0, expected: 254},
		{b: 255, index: 1, expected: 253},
		{b: 255, index: 2, expected: 251},
		{b: 255, index: 3, expected: 247},
		{b: 255, index: 4, expected: 239},
		{b: 255, index: 5, expected: 223},
		{b: 255, index: 6, expected: 191},
		{b: 255, index: 7, expected: 127},
	}

	for idx, tc := range tcs {
		ret := SetZeroBit(tc.b, tc.index)
		if ret != tc.expected {
			t.Errorf("#%v: SetZeroBit(%v, %v) = %v. Expected: %v", idx, tc.b, tc.index, ret, tc.expected)
		}
	}
}

var _dumpSetZeroBit byte

func BenchmarkSetZeroBit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_dumpSetZeroBit = SetZeroBit(0, 0)
	}
}

func TestFlipBit(t *testing.T) {
	tcs := []struct {
		b        byte
		index    byte
		expected byte
	}{
		// 0: 0000 0000
		{b: 0, index: 0, expected: 1},
		{b: 0, index: 1, expected: 2},
		{b: 0, index: 2, expected: 4},
		{b: 0, index: 3, expected: 8},
		{b: 0, index: 4, expected: 16},
		{b: 0, index: 5, expected: 32},
		{b: 0, index: 6, expected: 64},
		{b: 0, index: 7, expected: 128},
		// 57: 0011 1001
		{b: 57, index: 0, expected: 56},
		{b: 57, index: 1, expected: 59},
		{b: 57, index: 2, expected: 61},
		{b: 57, index: 3, expected: 49},
		{b: 57, index: 4, expected: 41},
		{b: 57, index: 5, expected: 25},
		{b: 57, index: 6, expected: 121},
		{b: 57, index: 7, expected: 185},
		// 255: 1111 1111
		{b: 255, index: 0, expected: 254},
		{b: 255, index: 1, expected: 253},
		{b: 255, index: 2, expected: 251},
		{b: 255, index: 3, expected: 247},
		{b: 255, index: 4, expected: 239},
		{b: 255, index: 5, expected: 223},
		{b: 255, index: 6, expected: 191},
		{b: 255, index: 7, expected: 127},
	}

	for idx, tc := range tcs {
		ret := FlipBit(tc.b, tc.index)
		if ret != tc.expected {
			t.Errorf("#%v: FlipBit(%v, %v) = %v. Expected: %v", idx, tc.b, tc.index, ret, tc.expected)
		}
	}
}

var _dumpFlipBit byte

func BenchmarkFlipBit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_dumpFlipBit = FlipBit(0, 0)
	}
}
