# Bit

Bit is the smallest unit which computer can understand and operate on.  
It's the representation of binary-based system so each bit can store 2 states: on/off, true/false or 1/0.

The typical operators can be used on bit are:
- AND (&): Return true only when both 2 bits are true, otherwise false.  
- OR (|): Return false only when both 2 bits are false, otherwise true.
- XOR (^): Return true only when both 2 bites a false, other false.
- NOT (!): Return opposite value of bit.
- LEFT-SHIFT (<<): Shift left x bits.
- RIGHT-SHIFT (>>): Shift right x bits.

#### Examples:
[...]
