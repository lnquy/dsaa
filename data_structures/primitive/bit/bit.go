package bit

// IsOneBit checks the value of bit at index is 1 or not.
func IsOneBit(b, index byte) bool {
	// (b >> index) & 0x01 == 1
	return (0x01<<index)&b != 0
}

// SetOneBit sets bit value at index to 1.
func SetOneBit(b, index byte) byte {
	return (0x01 << index) | b
}

// SetZeroBit sets bit value at index to 0.
func SetZeroBit(b, index byte) byte {
	// ((0xFF<<(index + 1)) | (0xFF >> (8 - index))) & b
	return ^(0x01 << index) & b
}

// FlipBit reverses the bit value at index.
func FlipBit(b, index byte) byte {
	//if IsOneBit(b, index) {
	//	return SetZeroBit(b, index)
	//}
	//return SetOneBit(b, index)
	return (0x01 << index) ^ b
}
