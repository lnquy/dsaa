package byte

import "testing"

func TestSum2Bytes(t *testing.T) {
	tcs := []struct {
		a byte
		b byte
		expected int16
	}{
		{a: 0, b: 0, expected: 0},
		{a: 1, b: 0, expected: 1},
		{a: 1, b: 1, expected: 2},
		{a: 32, b: 32, expected: 64},
		{a: 100, b: 50, expected: 150},
		{a: 255, b: 255, expected: 510},
		{a: 128, b: 255, expected: 383},
	}

	for idx, tc := range tcs {
		res := Sum2Bytes(tc.a, tc.b)
		if tc.expected != res {
			t.Errorf("#%d: Sum2Bytes(%d, %d) = %d. Expected: %d", idx, tc.a, tc.b, res, tc.expected)
		}
	}
}
