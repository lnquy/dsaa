package byte

var b byte

func Sum2Bytes(a, b byte) int16 {
	return int16(a) + int16(b)
}
