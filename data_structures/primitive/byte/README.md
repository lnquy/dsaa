# Byte
Byte is the smallest unit/type can be used by most programming languages.   
A byte is the group of 8 bits (octets) which means it can store up to 2^8 = 256 values.  
All other data types are built on top of byte(s). 

#### Examples
- Number 1 in deca is represented in binary and stored in 1 byte as: `00000001`.
- Number 2 in deca is represented in binary and stored in 1 byte as: `00000010`.
- Number 10 in deca is represented in binary and stored in 1 byte as: `00001010`.
- Number 255 in deca is represented in binary and stored in 1 byte as: `11111111`.  
255 is the biggest value can be store in 1 byte.

To store bigger value, just simply group up more bytes or in other word, use bigger data type.
- Number 256 in deca is represented in binary and stored in **2 bytes (int16)** as: `00000001 00000000`.

Some Go primitive data types built on byte:
- boolean: 1 byte
- char: 1 byte*
- int16/uint16: 2 bytes
- rune: 4 bytes*
- int32/uint32: 4 bytes
- int64/uint64: 8 bytes
- int/uint: 4 or 8 bytes depends on OS architecture.
- float: 8 bytes
- double: 16 bytes

\* depends on the character encoding, one character can be stored on 1 or multiples bytes.  
E.g.: ASCII requires 1 byte for each character but UTF-8 can use up to 4 bytes for each character (*code point* actually).  
In Go, string is encoded in UTF-8, so there's no actual `char` data type, instead we have `rune`.  
Each `rune` is the representation of one Unicode *code point* so it need [4 bytes for each rune](https://github.com/golang/go/blob/0dc814cd7f6a5c01213169be17e823b69e949ada/src/builtin/builtin.go#L92).

