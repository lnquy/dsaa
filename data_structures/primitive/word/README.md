# Word
Word is the number of bits processed by CPU in one cycle (typically 32/64 bits these days).  
Usually, the word size is equivalent to the length of CPU data bus so that a word of data can be moved from storage to a CPU register in single operation.  

Word may determine the way [memory organized](https://en.wikipedia.org/wiki/Data_structure_alignment). Many modern processors require memory to be organized on word size boundaries even for quantities smaller than the word size.  
Thus, modern programming languages often "optimize" the memory layout to fit in computer's word size to minimize the number of cycles to fetch data from RAM to CPU.    

For examples:
```go
type MyStruct struct {
	a int32   // 4 bytes
	b bool    // 1 byte
}
```
The total size of Go struct above is 5 bytes, but when running on a 32/64 bits computer, the actual size on memory is 8 bytes. 3 bytes was added as the padding bytes.
The memory layout of that struct on 32 bits architecture:
![x32](_misc/word_32.png)
and on 64 bits architecture:
![x64](_misc/word_64.png)


##### Notes:
- False sharing: https://en.wikipedia.org/wiki/False_sharing
