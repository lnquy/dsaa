# Page
Page (memory page, or virtual page) is a fixed-length contiguous block of **virtual memory**, described by a single entry in the [page table](https://en.wikipedia.org/wiki/Page_table). It is the smallest unit of data for memory management in a virtual memory operating system.  
Similarly, a page frame is the smallest fixed-length contiguous block of **physical memory** into which memory pages are mapped by the operating system.  
A transfer of pages between main memory and an auxiliary store, such as a hard disk drive, is referred to as paging or swapping.  
  
A common size of page is 4 KiB or 4096 bytes.

##### Notes:
- https://en.wikipedia.org/wiki/Page_(computer_memory)
